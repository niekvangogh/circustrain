# Circustrein

Hierbij de finale versie van de circustrein opdracht.

Deze opdracht is gemaakt in Java, dit had ik als eerst gelijk gedaan omdat er nergens stond dat het verplicht was dat het in C# gemaakt moest worden, ik heb deze opdracht logisch gemaakt en zou te volgen moeten zijn met beginnende C# kennis.

## Eisen

* Een dier dat van vlees houdt zal andere dieren opeten die van hetzelfde of een kleiner formaat zijn. Zorg er dus voor dat dit niet voorkomt.
* Elk klein dier telt voor 1 punt. Middelgrote dieren tellen zijn 3 en grote dieren zijn 5 punten. Elke treinwagon kan maximaal 10 punten vervoeren.
* Elke wagon moet optimaal benut zijn. Het is dus niet toegestaan om elk dier in een aparte wagon te plaatsen.

Voor elke eis, is er een unit test in het test project