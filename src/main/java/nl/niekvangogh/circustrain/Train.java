package nl.niekvangogh.circustrain;

import lombok.Getter;
import nl.niekvangogh.circustrain.animals.Animal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Train {

    private List<Wagon> wagons;

    @Getter
    private List<Animal> animals;

    public Train() {
        this.wagons = new ArrayList<>();
        this.animals = new ArrayList<>();
    }

    public List<Wagon> sortAnimals() {
        animals.sort(Comparator.comparing(Animal::getAnimalSize));
        this.wagons = new ArrayList<>();

        for (Animal animal : animals) {
            boolean added = false;
            for (Wagon wagon : this.wagons) {
                added = wagon.tryAddAnimal(animal);

                if (added) {
                    break;
                }
            }
            if (!added) {
                Wagon wagon = new Wagon();
                wagon.tryAddAnimal(animal);
                this.wagons.add(wagon);
            }
        }
        return this.wagons;
    }


}
