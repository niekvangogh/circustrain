package nl.niekvangogh.circustrain.animals;

import lombok.Getter;

public abstract class Animal {

    @Getter
    private AnimalSize animalSize;

    public Animal(AnimalSize animalSize) {
        this.animalSize = animalSize;
    }
}