package nl.niekvangogh.circustrain.animals;

public class Herbivorous extends Animal {

    public Herbivorous(AnimalSize size) {
        super(size);
    }
}
