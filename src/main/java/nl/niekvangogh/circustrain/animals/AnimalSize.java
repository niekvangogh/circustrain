package nl.niekvangogh.circustrain.animals;

import lombok.Getter;

public enum AnimalSize {

    SMALL(1), NORMAL(3), BIG(5);

    @Getter
    private int points;

    AnimalSize(int points) {
        this.points = points;
    }
}
