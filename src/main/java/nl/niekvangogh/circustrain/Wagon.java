package nl.niekvangogh.circustrain;

import lombok.Getter;
import lombok.Setter;
import nl.niekvangogh.circustrain.animals.Animal;
import nl.niekvangogh.circustrain.animals.AnimalSize;
import nl.niekvangogh.circustrain.animals.Carnivore;

import java.util.ArrayList;
import java.util.List;

public class Wagon {

    @Getter
    private int maxSize = 10;

    @Getter
    private List<Animal> animals;

    public Wagon() {
        this.animals = new ArrayList<>();
    }

    /**
     * @return int Returns the weight in {@link AnimalSize}
     */
    public int getSize() {
        return animals.stream().mapToInt(animal -> animal.getAnimalSize().getPoints()).sum();
    }

    /**
     * @return boolean Returns a boolean, corresponding on if the {@link Wagon} has a {@link Carnivore}.
     */
    public boolean hasCarnivore() {
        return this.animals.stream().anyMatch(animal -> animal instanceof Carnivore);
    }

    /**
     * @return Carnivore Returns the {@link Carnivore} in the {@link Wagon} if it contains one
     */
    public Carnivore getCarnivore() {
        if (this.hasCarnivore()) {
            return (Carnivore) this.animals.stream().filter(animal -> animal instanceof Carnivore).findAny().get();
        }
        return null;
    }

    /**
     * @param animal {@link Animal} which is to be added in the {@link Wagon}
     */
    public boolean tryAddAnimal(Animal animal) {
        if (WagonUtils.canAddAnimal(this, animal)) {
            this.animals.add(animal);
            return true;
        }
        return false;
    }
}