package nl.niekvangogh.circustrain;

import nl.niekvangogh.circustrain.animals.Animal;

public class AnimalUtils {

    /**
     * @param animal1 First animal to compare
     * @param animal2 Second animal to compare
     * @return boolean Checks which of the two animals is bigger
     */
    public static boolean isBigger(Animal animal1, Animal animal2) {
        return animal1.getAnimalSize().getPoints() > animal2.getAnimalSize().getPoints();
    }

    /**
     * @param animal1 First animal to be checked
     * @param animal2 Second animal to be checked
     * @return boolean Compares the two animals and checks if one of them is bigger or equal
     */
    public static boolean isBiggerOrEqual(Animal animal1, Animal animal2) {
        return animal1.getAnimalSize().getPoints() >= animal2.getAnimalSize().getPoints();
    }

}
