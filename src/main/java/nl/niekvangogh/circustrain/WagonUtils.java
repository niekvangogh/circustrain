package nl.niekvangogh.circustrain;

import nl.niekvangogh.circustrain.animals.Animal;
import nl.niekvangogh.circustrain.animals.Carnivore;

import java.util.Objects;

public class WagonUtils {


    /**
     * @param wagon  The wagon to check for
     * @param animal The animal which needs to be checked if it can be added
     * @return boolean Returns a boolean depending on if the animal can be added to the wagon, in here all the checks are done and made.
     */
    public static boolean canAddAnimal(Wagon wagon, Animal animal) {

        /*
         * Checks for if the AnimalSize is not bigger than the maximum of 10
         * */
        if (wagon.getSize() + animal.getAnimalSize().getPoints() > wagon.getMaxSize()) {
            return false;
        }

        /*
         * Checks if there are carnivores in the wagon already, and if so, check if the new animal is herbivour and bigger
         * */
        if (wagon.hasCarnivore()) {
            if (animal instanceof Carnivore) {
                return false;
            } else {
                return AnimalUtils.isBigger(animal, Objects.requireNonNull(wagon.getCarnivore()));
            }
        }

        /*
        * Checks if the carnivore can be added with the smaller animals
        * */
        if (animal instanceof Carnivore) {
            return wagon.getAnimals().stream().noneMatch(wagonAnimal -> AnimalUtils.isBiggerOrEqual(animal, wagonAnimal));
        } else {
            return true;
        }
    }
}
