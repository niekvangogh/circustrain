import nl.niekvangogh.circustrain.Train;
import nl.niekvangogh.circustrain.Wagon;
import nl.niekvangogh.circustrain.animals.AnimalSize;
import nl.niekvangogh.circustrain.animals.Carnivore;
import nl.niekvangogh.circustrain.animals.Herbivorous;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;

public class TrainTests {

    @Test
    public void WagonDoesNotGiveUnsafeAnimals_GivenTwoCarnivores_ExpectsTwoWagons() {
        Train train = new Train();
        train.getAnimals().add(new Carnivore(AnimalSize.NORMAL));
        train.getAnimals().add(new Carnivore(AnimalSize.SMALL));

        List<Wagon> wagons = train.sortAnimals();

        assertEquals("2 carnivores should not be together", 2, wagons.size());
    }

    @Test
    public void NoUnnecessaryWagonsAreCreated_GivenThreeAnimals_ExpectsTwoWagons() {
        Train train = new Train();
        train.getAnimals().add(new Carnivore(AnimalSize.NORMAL));
        train.getAnimals().add(new Carnivore(AnimalSize.SMALL));
        train.getAnimals().add(new Herbivorous(AnimalSize.BIG));

        List<Wagon> wagons = train.sortAnimals();

        assertEquals("2 wagons are expected and used optimal", 2, wagons.size());
    }

    @Test
    public void Test_AddAnimal_MaxWeightNotCrossed() {
        Train train = new Train();
        train.getAnimals().add(new Herbivorous(AnimalSize.BIG));
        train.getAnimals().add(new Herbivorous(AnimalSize.BIG));
        train.getAnimals().add(new Herbivorous(AnimalSize.SMALL));

        List<Wagon> wagons = train.sortAnimals();

        assertTrue("Wagon weights too much", wagons.get(0).getSize() < 10);
        assertTrue("Wagon weights too much", wagons.get(1).getSize() < 10);
    }
}